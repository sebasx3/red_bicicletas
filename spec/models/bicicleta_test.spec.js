var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error'));
        db.once('open', function() {
            console.log('Conectado a la base de datos MongoDB');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(error, success) {
            if (error) {
                console.error(error);
            }
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicletas.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            })
        });
    });


    describe('Bicicletas.add', () => {
        it('Agrega solo una bicicleta', (done) => {
            var bici = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicletas.findByCode', () => {
        it('Devuelve bici con codigo 1', (done) => {
            var bici = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err);
                var bici2 = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
                Bicicleta.add(bici2, function(err, newBici) {
                    if (err) console.log(err);
                    Bicicleta.findByCode(1, function(error, targetBici) {
                        expect(targetBici.code).toBe(bici.code);
                        expect(targetBici.color).toBe(bici.color);
                        expect(targetBici.modelo).toBe(bici.modelo);
                        done();
                    })
                });
            });
        });
    });

    describe('Bicicletas.removeByCode', ()=>{
        it('Elimina la bici con code 1', (done)=>{
            Bicicleta.find({},(err, bicis) => {
                expect(bicis.length).toBe(0);
      
                var aBici = new Bicicleta({ code:1, color:'violeta', modelo:'montaña' });
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    
                    var aBici2 = new Bicicleta({ code:2, color:'rojo', modelo:'urbana' });
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.removeByCode(1, function(errr, targetBici){
                            if(errr) console.log(errr);
                            Bicicleta.find({},(error,bicis) => {
                              expect(bicis.length).toBe(1);
                              done();
                            });
                        });
                    });
                });
            });
        });
      });
});

/*beforeEach(() => {
    Bicicleta.allBicis = [];
})

describe('Bicicleta.allBicis', () => {
    it('Comienza Vacía', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agregar una bicicleta', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'Rojo', 'Montañera', [12.1374736, -86.3056755]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Buscar una bicicleta con Id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'Rojo', 'Montañera', [12.1374736, -86.3056755]);
        Bicicleta.add(a);
        var b = new Bicicleta(2, 'Azul', 'Urbana', [12.1372616, -86.3197454]);
        Bicicleta.add(b);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(a.id);
        expect(targetBici.modelo).toBe(a.modelo);
        expect(targetBici.color).toBe(a.color);
    });
});

describe('Bicicleta.removeById', () => {
    it('Eliminar una bicicleta con Id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'Rojo', 'Montañera', [12.1374736, -86.3056755]);
        Bicicleta.add(a);
        var b = new Bicicleta(2, 'Azul', 'Urbana', [12.1372616, -86.3197454]);
        Bicicleta.add(b);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
});*/